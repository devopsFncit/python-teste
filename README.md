#!/usr/bin/env python3

from flask import Flask, render_template, request, Response
from config import mongo_db
import requests
from bson.json_util import dumps
import json


app = Flask(__name__)
API_ENDPOINT = "http://localhost:5000/criar"

@app.route("/")
def index():

        return render_template("index.html")



@app.route("/postendpoint", methods=["POST"])
def hello():
    nome = request.form['nomeform']
    email = request.form['emailform']
    data = '{"name": "%s", "email": "%s"}'% (nome, email)
    data1 = json.loads(data)

    print(data1["name"], data1["email"])

    r = requests.post(API_ENDPOINT, data = data1)
    resposta = r.text

    response = {"message": "Usuário '%s' criado com sucesso!"%(nome)}
    return Response(dumps(response), status=201, content_type="application/json")


@app.route("/criar", methods=["POST"])
def insertUser():
        try:
                user = request.get_json()
                mongo_db.user.insert_one(
                        {
                                "name": user["name"],
                                "email": user["email"]
                        }
                )
                response = {"message": "Usuário '%s' criado com sucesso!"%(user["name"])}
                return Response(dumps(response), status=201, content_type="application/json")

        except Exception as e:
                return "Erro %s"%(e)


if __name__ == "__main__":
        app.run(debug=True, host='0.0.0.0')




[root@fncsrvdyna teste]# cat config.py
from pymongo import MongoClient

client = MongoClient("mongodb://192.168.0.232:27017")
mongo_db = client.user
